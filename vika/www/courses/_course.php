<?php
echo "<a href='../'> Вернуться к списку курсов </a>";
connect_db();
$id_course=$_GET['id'];
//Получить информацию о курсе могут все юзеры
$query = "SELECT c.name as cname, c.extra_data, c.date_begin, c.date_end, c.active, u.id as uid, u.surname, u.name as uname, u.middlename
			  FROM course as c LEFT JOIN teacher as t ON c.id_teacher=t.id
			  LEFT JOIN user as u ON t.id_user=u.id
			  WHERE c.id=$id_course";
$uid_creator;
$res = mysql_query($query) or die(mysql_error());
if (mysql_num_rows($res) == 0)
	echo "Такого курса не существует";
else{
	$row = mysql_fetch_assoc($res);
	$uid_creator = $row['uid'];
	?>
	<li> Название курса: <?php echo $row['cname'];?></li>
	<li> Состояние курса: <?php echo ($row['active'] == 1)? "активен": "завешен" ;?></li>
	<li> Дополнительная информация о курсе: <?php echo $row['extra_data'];?></li>
	<li> Дата начала курса: <?php echo $row['date_begin'];?></li>
	<li> Дата окончания курса: <?php echo ($row['date_end'] == "")? "Дата окончания еще неизвестна": $row['date_end'];?></li>
	<li> Преподаватель курса: <?php echo $row['surname']." ".$row['uname']." ".$row['middlename'];?></li>
	<br>
	<?php
}

//Форма отправки
if ($uid_creator == $uid){		//Если текущий юзер и преподаватель группы совпадают, добавляем форму отправки сообщений
?>
	<p> Новое сообщение: </p>
	<form action="/courses/message_processing.php" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id_course" value=<?php echo $id_course;?>>
		<input type="hidden" name="id_user_sender" value=<?php echo $uid;?>>
		<textarea name="message" id="message_field" rows=10 cols=40></textarea>
		<input type="file" name="file" id="file_field"> 
		<input type="submit" value="Отправить">
	</form>
<?php
}

//Сообщения курса
$query = "SELECT m.body, m.date, m.id
		  FROM message as m 
		  WHERE id_course=$id_course
		  ORDER BY date DESC;";
if ($type == 1){		//Если зашел студент, подсветим ему непрочитанные
	//m.id - для filename
	$query = "SELECT m.body, m.date, m.id, cmr.id_user
			  FROM message as m 
			  LEFT OUTER JOIN course_message_received as cmr ON cmr.id_message=m.id
			  WHERE m.id_course=$id_course AND (cmr.id_user=$uid OR cmr.id_user IS NULL)
			  ORDER BY m.date DESC;";
}
$res = mysql_query($query) or die(mysql_error());
$count = mysql_num_rows($res);
//echo $count." - количество сообщений курса <br>";
if ($count == 0)
	echo "Сообщений в этом курсе пока нет.";
else "Сообщения курса:<br>";
while($row = mysql_fetch_assoc($res)){
	if ($row['id_user'] != 0){
		echo "ПОДСВЕЧИВАЕМ <br>";	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ПОДСВЕТИТЬ ТАКОЕ СООБЩЕНИЕ
	}
	?>
	<li> <?php echo $row['body'];?></li>
	<li> <?php echo "Дата отправки:".$row['date'];?></li>
	<?php
	//Ссылки на файлы сообщений
	$filename = $row['id'];
	$files = glob($root . "files/files_for_messages/$filename{.*,}", GLOB_BRACE);
	foreach($files as $file) {
		if ((file_exists($file))){		//Проверка лишней не будет.
			$path_file_for_user = '/files/files_for_messages/'.basename($file);
			$filename = basename($file);
			echo "<a href=$path_file_for_user> $filename </a><br><br>";		//Ссыль для скачивания
		}
	}
}

//Если в курс зашел студент, удаляем все его обновления по этому курсу
if ($type == 1){
	$query="DELETE FROM course_message_received
			WHERE id_user=$uid AND (id_message IN (SELECT id FROM message WHERE id_course=$id_course))";
//$query = "DELETE FROM course_message_received
//		  USING course_message_received as cmr LEFT JOIN message as m ON cmd.id_message=m.id
//		  WHERE cmr.id_user=$uid AND m.id_course=$id_course";		//Лажовый запрос.
	mysql_query($query) or die(mysql_error());
}
?>