<?php
echo "Список всех ваших курсов: <br>"; //.$uid

connect_db();
echo "<ul>";
if ($type == 1){		//Если юзер студент
	//Активные курсы
	$query = "SELECT c.id, c.name as course_name, u.surname, u.name, u.middlename
			  FROM student_course as sc LEFT JOIN student as s ON sc.id_student=s.id
			  LEFT JOIN course as c ON sc.id_course=c.id
			  LEFT JOIN teacher as t ON c.id_teacher=t.id
			  LEFT JOIN user as u ON t.id_user=u.id
			  WHERE s.id_user=$uid AND c.active=1;";
	$res = mysql_query($query) or die(mysql_error());
	if (mysql_num_rows($res) == 0)
		echo "У Вас нет активных курсов.";
	else echo "Список активных курсов:";
	while($row = mysql_fetch_assoc($res)){
		//print_r($row);
		?>
		<li><a href='<?php echo $row['id'];?>/'> Название курса: <?php echo $row['course_name'];?> | Преподаватель курса: <?php echo $row['surname'];?>, <?php echo $row['name'];?>, <?php echo $row['middlename'];?> </a></li>
		<?php
	}
	//Завершенные (неактивные) курсы
	$query = "SELECT c.id, c.name as course_name, u.surname, u.name, u.middlename
			  FROM student_course as sc LEFT JOIN student as s ON sc.id_student=s.id
			  LEFT JOIN course as c ON sc.id_course=c.id
			  LEFT JOIN teacher as t ON c.id_teacher=t.id
			  LEFT JOIN user as u ON t.id_user=u.id
			  WHERE s.id_user=$uid AND c.active=0;";
	$res = mysql_query($query) or die(mysql_error());
	if (mysql_num_rows($res) == 0)
		echo "У Вас нет завершенных курсов.";
	else echo "Список завершенных курсов:";
	while($row = mysql_fetch_assoc($res)){
		//print_r($row);
		?>
		<li><a href='<?php echo $row['id'];?>/'> Название курса: <?php echo $row['course_name'];?> | Преподаватель курса: <?php echo $row['surname'];?>, <?php echo $row['name'];?>, <?php echo $row['middlename'];?> </a></li>
		<?php
	}
}

if ($type == 2){		//Если юзер преподаватель
	//Активные курсы
	$query = "SELECT c.id, c.name as course_name
			  FROM course as c LEFT JOIN teacher as t ON c.id_teacher=t.id
			  WHERE t.id_user=$uid AND c.active=1;";
	$res = mysql_query($query) or die(mysql_error());
	if (mysql_num_rows($res) == 0)
		echo "У Вас нет активных курсов.";
	else echo "Список активных курсов:";
	while($row = mysql_fetch_assoc($res)){
		?>
		<li><a href='<?php echo $row['id'];?>/'> Название курса: <?php echo $row['course_name'];?> </a></li>
		<?php
	}
	//Завершенные (неактивные) курсы
	$query = "SELECT c.id, c.name as course_name
			  FROM course as c LEFT JOIN teacher as t ON c.id_teacher=t.id
			  WHERE t.id_user=$uid AND c.active=0;";
	$res = mysql_query($query) or die(mysql_error());
	if (mysql_num_rows($res) == 0)
		echo "У Вас нет завершенных курсов.";
	else echo "Список завершенных курсов:";
	while($row = mysql_fetch_assoc($res)){
		?>
		<li><a href='<?php echo $row['id'];?>/'> Название курса: <?php echo $row['course_name'];?> </a></li>
		<?php
	}
}
if ($type == 3){		//Если юзер админ и попал случайно, набрав url, предназначенный не для него
	echo "Вы администратор, у Вас нет никаких курсов.";
}
echo "</ul>";
?>