<?php
function get_hash($salt, $password){
    return md5($salt . $password . "YmenyaVkomnateZivetDerevo");
}

function redirect($url, $statusCode = 303)
{
	header('Location: ' . $url, true, $statusCode) or die();
}

function connect_db(){
	//$host, $username, $password, $db_name настраиваемые
	//Коннектимся
	global $host, $username, $password, $db_name;
	mysql_connect("$host", "$username", "$password") or die("Не могу соединиться с MySQL"); 
	mysql_select_db("$db_name") or die("Не могу подключиться к базе");
}

//Перед вызовом этой ф-ии ОБЯЗАТЕЛЬНО session_start();
function check_user_login(){	//Возвращает 1, если юзер уже залогинен
	connect_db();		//Подключение к БД
	session_start();
	$query = "SELECT s.date_begin, u.type, u.id FROM user_session_code as s LEFT JOIN user as u ON u.id = s.id_user WHERE id_user = '".$_SESSION['id_user']."' and session_code = '".$_SESSION['session_code']."';";	//Найти нужные: соль и хэш для проверки правильности пароля

	$res = mysql_query($query) or die(mysql_error());			//Выполняем запрос
	$count = mysql_num_rows($res);	//Количество строк, выданное базой
	if ($count == 0){
		mysql_close();
		return 0;		//0 строк в запросе
	}
	$row = mysql_fetch_assoc($res);			//Берем первую строчку из результатов(единственную)
	$type = $row['type'];					//Получаем тип пользователя
	$uid = $row['id'];					//Получаем id'шник пользователя
	$date = $row['date_begin'];				//Получаем дату начала/последнего обновления сессии
	$now = date("Y-m-d H:i:s");
	//$now = "2015-05-17 03:03:17";  тест
	$diff = strtotime($now) - strtotime ($date);	//Получили разницу в секундах
	if ($diff < 86400){		//86400 секунд в сутках
		mysql_query("UPDATE user_session_code set date_begin='{$now}' where id_user={$uid};") or die(mysql_error());	
		mysql_close();
		return array($type, $uid);			//Все ок, юзер залогинен
	}
		
	//Если запись хранится тут больше суток, удаляем ее
	$query = "DELETE FROM `user_session_code` WHERE `id_user`='".$_SESSION['id_user']."' and `session_code`='".$_SESSION['session_code']."';";
	$res = mysql_query($query) or die(mysql_error());
	mysql_close();
	return 0;	//Просроченная запись, она удалена
}


function user_fio_type($id_user){		//Поиск по id в таблице user.  Выдает фамилию и инициалы. + права юзера
	// преподавателя его фамилии и инициалов
	$query = "SELECT surname, name, middlename, type FROM user WHERE id = $id_user;";		//ФИО препода по его id
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);
	$type = $row['type'];
	$surname = $row['surname'];
	$name = $row['name'];	
	$middlename = $row['middlename'];
	$i = substr($name, 0, 2);
	$o = substr($middlename, 0, 2);
	$fio = "$surname $i.$o.";
	return array($fio, $type);
}

function search_num_ag($id_user){		//Найти номер группы по id юзера
	$query = "SELECT number FROM student as s LEFT JOIN academ_group as ag ON s.id_ag = ag.id 
			  WHERE s.id_user=$id_user";
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);
	$number = $row['number'];
	return $number;
}

function userdata($id_user){
	$query = "SELECT surname, name, middlename, type, active FROM user
				  WHERE id=$id_user";
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);

	$count = mysql_num_rows($res);	//Количество строк, выданное базой
	if ($count == 0)
		return 0;		//0 строк в запросе
	
	$surname = $row['surname'];
	$name = $row['name'];
	$middlename = $row['middlename'];
	$type = $row['type'];
	$active = $row['active'];
	
	return array($surname, $name, $middlename, $type, $active);
}

function teacherdata($id_user){		//Найти данные тичера группы по id юзера
	$query = "SELECT extra_data FROM teacher
			  WHERE id_user=$id_user";
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);
	$extra_data = $row['extra_data'];
	return $extra_data;
}

function studentdata($id_user){		//Найти данные студента по id юзера
	$query = "SELECT * FROM student as s LEFT JOIN academ_group as ag ON s.id_ag = ag.id
			  WHERE s.id_user=$id_user";
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);
	$number = $row['number'];
	$date_begin = $row['date_begin'];
	$type = $row['type'];
	
  $number += (date('Y') - $date_begin + ((date('m') > 8) ? 1 : 0)) * 100;
	
	return array($number, $type);
}

function courseactive_data($id_user, $active){		//Найти $active(активлые/неактивные) курсы студента по id юзера
	$query = "SELECT c.name FROM student_course as sc LEFT JOIN student as s ON sc.id_student=s.id
			  LEFT JOIN course as c ON sc.id_course=c.id
			  LEFT JOIN teacher as t ON c.id_teacher=t.id
			  LEFT JOIN user as u ON t.id_user=u.id
			  WHERE s.id_user=$id_user AND c.active=$active;";
	$res = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_assoc($res);

	$sc_date_begin = $row['sc.date_begin'];
	$mark = $row['sc.mark'];
	
	$course_id = $row['c.id'];
	$course_name = $row['c.name'];
	$extra_data = $row['c.extra_data'];
	$c_date_begin = $row['c.date_begin'];
	$c_date_end = $row['c.date_end'];
	$type_mark = $row['c.type_mark'];
	
	$surname = $row['u.surname'];
	$name = $row['u.name'];
	$middlename = $row['u.middlename'];
	
	return array($sc_date_begin, $mark, $course_id, $course_name, $extra_data, $c_date_begin, $c_date_end, $type_mark, $surname, $name, $middlename);
}
/*
course(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	id_teacher INT UNSIGNED NOT NULL,
	extra_data text,
	date_begin DATE NOT NULL,
	date_end DATE NOT NULL,
student(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_user INT UNSIGNED NOT NULL UNIQUE,
	id_ag INT UNSIGNED NOT NULL,
	FOREIGN KEY (id_ag) REFERENCES academ_group (id)
student_course(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_student INT UNSIGNED NOT NULL,
	id_course INT UNSIGNED NOT NULL,
	date_begin DATE NOT NULL,
	date_end DATE NOT NULL,
	reason_exit TINYINT,
	ball SMALLINT,
	*/
?>
