﻿<!DOCTYPE html>
<html>
<head>
	<title>Создание БД</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
$host="localhost";	//Имя хоста
$username="root";	//Mysql username 
$password="";		//Mysql пароль
$db_name="db_main";

mysql_connect("$host", "$username", "$password")or die(mysql_error());

// Создание базы данных
mysql_query("CREATE DATABASE $db_name") or die(mysql_error());

//Создание таблиц
mysql_select_db("$db_name")or die(mysql_error());
echo "<pre>";
echo "Созданы таблицы:\n";
//__________________________________________________________________
//__________________________________________________________________
mysql_query("CREATE TABLE user(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	login VARCHAR(20) NOT NULL UNIQUE,
	password_hash VARCHAR(40) NOT NULL,
	salt VARCHAR(40) NOT NULL,
	surname VARCHAR(20) NOT NULL,
	name VARCHAR(20) NOT NULL,
	middlename VARCHAR(20) NOT NULL,
	type TINYINT NOT NULL,
	active TINYINT NOT NULL
)") Or die(mysql_error());
//type - 1) student 2) teacher 3) admin
//active - 1) активен 2) не активен	
echo "user\n";

//__________________________________________________________________

mysql_query("CREATE TABLE `user_session_code`(
	id_user INT UNSIGNED NOT NULL,
	session_code VARCHAR(40) NOT NULL,
	date_begin DATETIME NOT NULL,
	PRIMARY KEY (id_user, session_code),
	FOREIGN KEY (id_user) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)") Or die(mysql_error());
echo "user_unique_session_code\n";
//__________________________________________________________________
//Академические группы
mysql_query("CREATE TABLE academ_group(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	number SMALLINT NOT NULL,
	date_begin SMALLINT NOT NULL,
	type TINYINT NOT NULL
)") Or die(mysql_error());
//type - 1) бакалавр, 2) магистр, 3) специалист
echo "academ_group\n";

//Студенты
mysql_query("CREATE TABLE student(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_user INT UNSIGNED NOT NULL UNIQUE,
	id_ag INT UNSIGNED NOT NULL,
	FOREIGN KEY (id_ag) REFERENCES academ_group (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
    FOREIGN KEY (id_user) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE	
)") Or die(mysql_error());
//    photo VARCHAR(100), - Хранится в папке "Files" под именем логина, если нет, то стандартная фотография
echo "student\n";
//__________________________________________________________________
mysql_query("CREATE TABLE teacher(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_user INT UNSIGNED NOT NULL UNIQUE,
	extra_data VARCHAR(10000),
	FOREIGN KEY (id_user) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE	
)") Or die(mysql_error());
//    photo VARCHAR(100), - Хранится в папке "Files" под именем логина, если нет, то стандартная фотография
echo "teacher\n";



//Курсы
mysql_query("CREATE TABLE course(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	id_teacher INT UNSIGNED NOT NULL,
	extra_data VARCHAR(10000),
	date_begin DATE NOT NULL,
	date_end DATE,
	type_mark boolean NOT NULL,
	active BOOLEAN NOT NULL,
	FOREIGN KEY (id_teacher) REFERENCES teacher (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)") Or die(mysql_error());
echo "course\n";
//date_end определится, когда преподаватель сам завершит свой курс
//type_mark - 0 от 2 до 5-ти, 1 зачет/незачет: 2/3

mysql_query("CREATE TABLE student_course(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_student INT UNSIGNED NOT NULL,
	id_course INT UNSIGNED NOT NULL,
	date_begin DATE NOT NULL,
	mark TINYINT,
	FOREIGN KEY (id_student) REFERENCES student (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (id_course) REFERENCES course (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)") Or die(mysql_error());
//reason_exit - 1) покидание (выбор другого, попадание в другу подгруппу и т.п.), 2) аттестация, 3) завершение курса (если курс не требует отчетности по иным причинам).
echo "student_course\n";

//Сообщения
mysql_query("CREATE TABLE message(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_course INT UNSIGNED,
	id_user_sender INT UNSIGNED NOT NULL,
	body VARCHAR(10000),
	date DATETIME NOT NULL,
	FOREIGN KEY (id_course) REFERENCES course (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	FOREIGN KEY (id_user_sender) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)") Or die(mysql_error());
echo "message\n";

mysql_query("CREATE TABLE file(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_message INT UNSIGNED NOT NULL,
	name VARCHAR(100) NOT NULL,
	file_path BIGINT UNSIGNED UNIQUE
)") Or die(mysql_error());
//В пути храним уникальное имя - цифру; Файлы хранятся в папке "Files"
echo "file\n";

mysql_query("CREATE TABLE message_receiver(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_message INT UNSIGNED NOT NULL,
	id_user_receiver INT UNSIGNED NOT NULL,
	received BOOLEAN NOT NULL,
	FOREIGN KEY (id_message) REFERENCES message (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (id_user_receiver) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)") Or die(mysql_error());
echo "message_receiver\n";

mysql_query("CREATE TABLE course_message_received(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	id_message INT UNSIGNED NOT NULL,
	id_user INT UNSIGNED NOT NULL,
	FOREIGN KEY (id_message) REFERENCES message (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (id_user) REFERENCES user (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)") Or die(mysql_error());
echo "course_message_received\n";

//mysql_query("") Or die(mysql_error());

//mysql_query("") Or die(mysql_error());

mysql_close();
echo "База данных успешно создана.";
echo "</pre>";
?>
</body>
