<?php
	
	require_once  ('../config.php');
	require_once  ($root.'/functions/func.php');

	if ($type = check_user_login()){	//Если пользователь залогинен, то смотрим его тип и отправляем его на его страницу
		header("Location: /");
	}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Вход</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="style.css" type="text/css" />
  <style>
    body {
      font-size:18px;
      min-width:14em;
      width:14em;
      min-height:9em;
      height:9em;
      margin:5% auto 0;
      padding:2em;
      border-width:2px;
      border-radius:20px;
      color:#555;
      font-family:sans-serif;
    }

    #login dl {
      overflow:hidden;
      padding:1em 0 2em;
      margin:0;
    }

    #login dl dt ,
    #login dl dd {
      float:left; 
      padding:0.2em 0;
      margin:0;
    }
    #login dl dt ,
    #login dl dd ,
    #login dl dd input {
      width:7em;
    }  

    #login dl dt {
      clear: left;
    }

    #login form input{
      font-size:17px;
    }

    #login form input[type=submit]{
      border:0;
      background-color:#D60;
      padding:0.3em 0.6em;
      color:#FFF;
      float:right;

    }
  </style>
</head>
<body>
  <div id="login">
    <form name="form1" method="post" action="validation.php"> <!-- -->
      <dl>
        <dt>Логин: </dt>
        <dd>
          <input type="text" name="login" id="login-field" value="TeAcher"/><!-- -->
        </dd>
        <dt>Пароль: </dt>
        <dd>
          <input type="password" name="password" id="password-field" value="catcat"/><!-- -->
        </dd>
      </dl>
      <input type="submit" name="Submit" value="Войти"/><!-- -->
    </form>
  </div>
</body>
</html>
