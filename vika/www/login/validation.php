<?php
	require_once  ('../config.php');
	require_once  ($root.'/functions/func.php');

connect_db();	//Подключаемся к БД

//Получаем из формы логин и пароль
$login=$_POST['login']; 
$password=$_POST['password']; 

$host  = $_SERVER['HTTP_HOST'];		//Будет использоваться в header

$login=strtolower($login);				//Перевод в нижний регистр
if (!(preg_match ("/^[a-z0-9_]{6,20}$/", $login) && preg_match ("/^[a-zA-Z0-9]{6,20}$/", $password))){	//Проверка корректности введенных данных
	redirect("/login");
}
$sql="SELECT `id`, `salt`, `password_hash`, `type`, `active` FROM `user` where `login`='$login'";	//Для проверки правильности пароля
$res=mysql_query($sql) or die(mysql_error());		//Выполняем запрос
$count=mysql_num_rows($res);						//Количество строк, выданное базой
//Если количество возвращенных БД значений == 0 (в БД нет подходящих логина и пароля)
if ($count==0){
	redirect("/login");		//Возврат к странице логина
}
$row = mysql_fetch_assoc($res);			//Берем первую строчку из результатов(единственную)
$id_user = $row['id'];
$salt=$row['salt'];						//Получаем соль
$password_hash=$row['password_hash'];	//Получаем хэш
$type = $row['type'];					//Получаем тип
$active=$row['active'];					//Получаем активность
//Проверка активности пользователя
if ($active!=1){	//Если пользователь деактивирован
	redirect("/login");		//Возврат к странице логина
}
//Проверяем корректность введенных данных и активность юзера, создаем запись с уникальным кодом сессии в табл. user_session_code

if (get_hash($salt,$password)==$password_hash){	//Успех!
	session_start();
	$session_code = sha1(mt_rand());
	$date=date("Y-m-d H:i:s"); 
	$query="insert into $db_name.user_session_code (id_user, session_code, date_begin) values ('$id_user', '$session_code', '$date');";
	mysql_query($query) or die(mysql_error());
	mysql_close();
	
	$_SESSION['id_user'] = $id_user;	//регистрируем переменные
	$_SESSION['session_code'] = $session_code;
	
	redirect("/");
}	
mysql_close();

redirect("/login");		//Возврат на страницу логина
?>
