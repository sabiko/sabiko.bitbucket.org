<!DOCTYPE html>
<html>
<head>
  <title>Мои курсы</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body>
  <div id="header">
    <ul id="main-menu">
      <li><a href="#1">Главная</a></li>
      <li><a href="#2">Сообщения</a></li>
      <li class="current"><span>Мои курсы</span></li>
      <li><a href="#3">Календарь</a></li>
      <li class="logout"><a href="">Выход</a></li>
    </ul>
  </div>
  <div id="content">
    <h1> Мои курсы</h1>
<!-- До этого места header.php
     Title + h1 - переменная, задаём перед вызовом хедера 
     Активный пункт меню - тоже 
 -->
    <ul>
      <li><a href="#">Математический анализ <span>Додонов Н. Ю.</span></a></li>
      <li><a href="#">Операционные системы <span>Вояковская</span></a></li>
      <li><a href="#">Геометрия <span>Пилюгин П.Д.</span></a></li>
    </ul>

<!-- отсюда footer.php -->
  </div>
</body
</html>