<p>Тут список диалогов:</p> 
<ul id="dialogs">

<?php
  connect_db();
  $query = "select dialog.received, dialog.date, dialog.body, dialog.rid, u.type, u.name, u.surname, ag.number, ag.date_begin from (
    select m.id, mr.received, date, body, mr.id_user_receiver as rid from message as m left join message_receiver as mr on m.id=mr.id_message where m.id_user_sender=$uid
    union
    select m.id, mr.received, date, body, m.id_user_sender as rid from message as m left join message_receiver as mr on m.id=mr.id_message where mr.id_user_receiver=$uid
    order by date desc
  ) as dialog
  left join user as u on dialog.rid=u.id
  left join student on u.id=student.id_user
  left join academ_group as ag on ag.id=student.id_ag
  group by rid order by date desc;";

  $res = mysql_query($query) or die(mysql_error());

  while($row = mysql_fetch_assoc($res)){		//Пока есть строчки
?>
  <li class="message">
    <div>
      <a href="/profile/<?php echo $row['rid'] ?>">
        <?php echo $row['surname']." ".$row['name']; ?> 
      </a>
      <?php
        $utype = $row['type'];
        if ($utype == 3)
          echo "<span>Администратор</span>";
        elseif ($utype == 1){
          $ag_num = $row['number'] + (date('Y') - $row['date_begin'] + ((date('m') > 8) ? 1 : 0)) * 100;
          echo "<span>$ag_num - {$row['date_begin']}</span>";
        }
      ?>
      <span><?php echo $row['date']; ?></span>
    </div>
    <p>
      <a href="/messages/<?php echo $row['rid'] ?>" <?php if ($row['received']) echo "class=\"new\""; ?> >
        <?php echo $row['body']; ?>
      </a></p>
  </li>

<?php	
}
mysql_close();
?>
</ul>
