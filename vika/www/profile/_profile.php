<?php
connect_db();

//$id_profile=$_GET['uid'];
$pid = ($id_profile != null)? $id_profile : $uid;		//Юзер, которого надо показать

if (!(list($surname, $name, $middlename, $type, $active) = userdata($pid)) || $active == 2)	//Если 0, нет результатов по запросу или юзер деактивирован
	echo "Этого пользователя не существует";
else{
	//Подгрузить фотографию
	$filename = "/files/photos/$pid.jpg";
	
	if (!file_exists($root."files/photos/$pid.jpg"))		//Если файл не существует
		$filename = "/files/photos/0.jpg";					//Файл со стандартной фотографией
		
?>
	<img src="<?php echo $filename ?>" width=300 alt="У вас нет фотографии">
<?php

	if ($pid == $uid){		//Добавляем возможность загрузить файл с фотографией
		//require_once  ('./form_add_photo.php');
		?>
		<div id="input_photo">
		<form name="form1" enctype="multipart/form-data" method="post" action="upload_photo.php"> <!-- -->
		  <dl>
			<dd>
				<input type="hidden" name="uid" value=<?php echo $uid;?>>
			</dd>
			<dd>
				<input type="hidden" name="MAX_FILE_SIZE" value="5242880" />
			</dd>
			<dt>Сменить фото:</dt>
			<dd>
			  <input type="file" name="userphoto" id="input_field" accept=".jpg,.png"> 
			</dd>
		  </dl>
		  <input type="submit" name="Submit" value="Сменить"/><!-- -->
		</form>
	  </div>
	  <?php
	}
	
	echo "<br>$surname, $name, $middlename <br>";
	
	if ($type == 1){		//Если студент
		echo "Статус: студент. <br>";
		list($number, $num_type) = studentdata($pid);
		echo "Номер группы:$number <br>";
		if ($num_type == 1)
			echo "Бакалавр <br>";
		if ($num_type == 2)
			echo "Магистр <br>";
		if ($num_type == 3)
			echo "Специалитет <br>";
	}
	if ($type == 2){		//Если преподаватель
		echo "Статус: преподаватель. <br>";
		$extra_data = teacherdata($pid);
		echo "Доп. данные: $extra_data <br>";
	}
	if ($type == 3){		//Если администратор
		echo "Статус: администратор. <br>";
	}

}


/*
		<dd>
			<input type="hidden" name="MAX_FILE_SIZE" value="10000" />
		</dd>

  <div id="search">
    <form name="form1" method="post" action="search_profile.php"> <!-- -->
      <dl>
        <dt>Фамилия: </dt>
        <dd>
          <input type="text" name="surname" id="surname-field"/><!-- -->
        </dd>
        <dt>Имя: </dt>
        <dd>
          <input type="text" name="name" id="name-field"/><!-- -->
        </dd>
		<dt>Отчество: </dt>
        <dd>
          <input type="text" name="middlename" id="middlename-field"/><!-- -->
        </dd>
      </dl>
      <input type="submit" name="Submit" value="Войти"/><!-- -->
    </form>
  </div>*/
?>